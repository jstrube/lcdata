function kochsnowflake(iters)
  rot = [cos(pi/3) -sin(pi/3); sin(pi/3) cos(pi/3)]
  len = 2 + 4^iters - 1
  pts = Array(Float64, 2, len)
  pts[:,1] = [0, 0]
  pts[:,len] = [1, 0]
  stride=len-1
  for i=1:iters
    nextstride = div(stride, 4)
    for j=1:stride:len-1
      disp = pts[:,j + stride] - pts[:,j]
      pts[:,j + nextstride] = pts[:,j] + disp/3
      pts[:,j + 2*nextstride] = pts[:,j + nextstride] + rot * (disp/3)
      pts[:,j + 3*nextstride] = pts[:,j] + disp * 2/3

    end
    stride = nextstride
  end
  return pts
end
