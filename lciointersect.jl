include("LCIOUtils.jl")

const filename = ARGS[1]
const eventstart = parse(Int, ARGS[2])
const eventend = parse(Int, ARGS[3])

using MeshTools, DataStructures, LCIOUtils
type Intersector
    particle :: Int
    event :: Int
    meshes :: LinkedList{Mesh}
    types :: LinkedList{Int}
    Intersector() = new(1, 0, nil(Mesh), nil(Int))
end

function saveresults(intersector)
    hcyl = invertnormals(cylinder(hcal_outer, hcal_length, 64))
    ecyl = invertnormals(cylinder(ecal_outer, ecal_length, 64))
    ecyl2 = cylinder(ecal_inner, ecal_length, 64)
    v, ind = joinmeshes(ecyl, ecyl2, hcyl, intersector.meshes...)

    #TODO: Write the moments of inertia of each type of particle
    for (particleID, (v2, ind2)) in zip(intersector.types, intersector.meshes)
        inertia = inertiatensor(v2)
        F = eigfact(inertia)
        append("$(particleID).obj", )
    end

    saveObj(v, ind, "meshes2/event$(intersector.event).obj")
    intersector.meshes = nil(Mesh)
end

function call(intersector :: Intersector, positions :: Matrix, event :: Int, maxEnergy :: Float64, momentum :: Vector, particleID)
    numhits = size(positions, 2)
    if numhits == 0
        println("no points to load at event $(event)!")
        maxEnergy = -1
    else
        println("processing $particleID at event $event with $numhits hits... ")

        if event == intersector.event
            intersector.particle += 1

        else
            if intersector.event > 0
                saveresults(intersector)
            end
            intersector.particle = 1
            intersector.event = event
        end
        mesh = convexhull(positions[1:3, :])
        # saveObj(mesh..., "meshes/event$(intersector.event)-particle$(intersector.particle).obj")
        intersector.meshes = cons(mesh, intersector.meshes)
        intersector.types = cons(particleID, types)
    end

end
intersector = Intersector()
mapparticles(intersector, filename, eventstart, eventend)
saveresults(intersector)
