include("lcioanalyze.jl")
using Gadfly
if length(ARGS) != 4
    println("Usage: PROGRAM filename plotpath lowerbound upperbound")
    exit()
end

gaussmodel(x, p) =p[1]*exp(-(x-p[2]).^2/(2*p[3]^2))

"simple RMS90 of a set of values, using distance from the mean"
function rms90(v::Vector)
    len = length(v)
    len -= div(len, 10)
    distanceFromMean = abs(v - mean(v))
    sort!(distanceFromMean)
    return std(distanceFromMean[1:len])
end

"RMS90 using iterative method, given an existing histogram"
function rms90(es, counts)
    total = sum(counts)
    mids = (es[1:end-1] + es[2:end])/2
    midc = mids .* counts
    midc2 = midc .* mids
    rawrms = mean(midc2) - mean(midc) ^ 2

    is0 = 0
    binSum = 0
    iBin = 1
    for iBin=1:length(counts)
        binSum += counts[iBin]
        is0 = iBin
        if binSum > 0.1*total
            break
        end
    end
    rmsmin = 9999
    xmean = 0
    xrms = 0
    sigma = 0
    sigmasigma = 0
    xlow = Inf
    xhigh = -Inf
    for istart=1:is0+1
        sumn = 0
        csum = 0
        sumx = 0
        sumxx = 0
        iend = 0
        iBin = istart
        for iBin=1:length(counts)
            binx = (es[iBin] + es[iBin+1])/2
            yi = counts[iBin]
            csum += yi
            if sumn < 0.9*total
                sumn += yi
                sumx += yi*binx
                sumxx += yi*binx*binx
                iend = iBin
            end
        end
        meann = sumx/sumn
        meannsq = sumxx/sumn
        rms = sqrt(meannsq - meann^2)
        if rms < rmsmin
            xmean = meann
            xrms = rms
            xlow = es[istart]
            xhigh = es[iend]
            rmsmin = rms
        end
    end
    return xmean, xrms, xlow, xhigh, total
end

function gaussfit(x, n=div(length(x), 10))

    es, counts = hist(x, n)
    topcount, i = findmax(counts)
    topes = es[i]

    es = (es[1:end-1])

    stdev = std(x)
    println(stdev)
    fit = curve_fit(gaussmodel, es, counts, Float64[topcount, topes, stdev])
    return fit.param
end

path = ARGS[1]
plotpath = ARGS[2]
lo = parse(Int, ARGS[3])
hi = parse(Int, ARGS[4])
df = makedensityframe(path)
lowfit = gaussfit(df[:Low])
highfit = gaussfit(df[:High])
allfit = gaussfit(df[:All])
# println(lowfit.resid)
# println(highfit.resid)
# println(allfit.resid)
p = plot(df, x="Low", Geom.histogram, Guide.title("Low density std: $(lowfit[3])"), Scale.x_continuous(minvalue=lo, maxvalue=hi))
p2 = plot(df, x="High", Geom.histogram, Guide.title("High density std: $(highfit[3])"), Scale.x_continuous(minvalue=lo, maxvalue=hi))
p3 = plot(df, x="All", Geom.histogram, Guide.title("All energies std: $(allfit[3])"))
draw(PNG("$(plotpath)$(basename(path))plot.png", 18cm, 18cm), vstack(p, p2, p3))
