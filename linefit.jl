function fitline(points::Matrix)
  centroid = mean(points, 2)[:,1]
  M = transpose(points - repeat(centroid, inner=[1, size(points)[2]]))
  U, S, V = svd(M)
  n = V[:, 1]
  return centroid, n
end
