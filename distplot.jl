include("lcioanalyze.jl")
using HDF5
using Gadfly

path = ARGS[1]
plotpath = ARGS[2]

for particlename = ["pi-", "mu+", "gamma", "e-", "K0L"]
  for energy = [1, 2, 5, 10, 20, 50, 100]
    name = getdistfilename(particlename, energy, path)
    dist = h5read(name, "dist")
    maxrad = h5read(name, "maxrad")
    mindist = h5read(name, "mindist")
    maxdist = h5read(name, "maxdist")
    dir = h5read(name, "dir")
    map!(x->round(x, 4), dir)
    # println(dist)
    p = plot(z=transpose(dist), y=linspace(0, maxrad, size(dist)[1]), x=linspace(mindist, maxdist, size(dist)[2]), Geom.contour, Guide.title("Direction: ($(dir[1]), $(dir[2]), $(dir[3]))"), Guide.xlabel("slice"), Guide.ylabel("radius"))
    draw(PNG("$(plotpath)$(particlename)-$(energy).png", 18cm, 9cm), p)
  end
end
