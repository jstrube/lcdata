include("lcioanalyze.jl")
using Gadfly
const blur = 4.0
const plotpath = "/Users/james/Documents/SULI/lcdata/densitydistplots/"
const minx = 0
const maxx = 20
plots = Array(Gadfly.Plot, length(allparticles), length(allenergies))
for (i, name) in enumerate(allparticles)
    for (j, energy) in enumerate(allenergies)
        df = getparticledensitydist(name, energy, "/Users/james/Documents/SULI/lcdata/densityoutput/", blur, 1, 1000)
        p = plot(get(df), x="Density", Geom.histogram, Guide.title("$energy GeV $name, blur $blur"), Scale.x_continuous(minvalue=minx, maxvalue=maxx))
        plots[i, j] = p
        # draw(PNG("$(plotpath)$name-$energy-$blur.png", 18cm, 18cm), p)
    end
end

energyplots = mapslices(x->vstack(x...),plots, 1)
allplots = hstack(energyplots...)
draw(PNG("$(plotpath)all_density_dists-blur_$blur.png", 36cm, 36cm), allplots)
