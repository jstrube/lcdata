include("lcioanalyze.jl")
using MeshTools
using Gadfly

function printexit()
    println("USAGE: PROGRAM datapath plotpath x-axis [y-axis] [-s]")
    println("where x-axis and y-axis are one of: HullDensity, MeshDensity, HullEccentricity, MeshEccentricity, MeshParts, VolumeRatio, Inertia, FractalDimension")
    println("When plotting two axes, -s creates separate plots for the ecal, hcal.")
    println("When plotting one axis, -s sets the x axis to log scale.")
    exit()
end

function plotparticle(name, xname, yname)
    getdf(df, name, energy) = df[(df[:Particle] .== name) & (df[:Energy] .== energy), :]
    function plothparticleenergy(energy, ind)
        newdf = nothing
        if ind == 0
            newdf = getdf(dfh, name, energy)
        elseif ind == 1
            newdf = getdf(dfe, name, energy)
        else
            newdf = getdf(dfb, name, energy)
        end
        size(newdf)[1] > 1 ? plot(newdf[(newdf[:Particle] .== name) & (newdf[:Energy] .== energy), :], x=xname, y=yname, Guide.title("$name, $energy GeV - Hcal"), Geom.histogram2d(xbincount=30, ybincount=30)) : plot(x=[0, 1], y=[0, 1])
    end
    plotsh = map(x->plothparticleenergy(x, 0), allenergies)
    plotse = map(x->plothparticleenergy(x, 1), allenergies)
    plotsb = map(x->plothparticleenergy(x, 2), allenergies)
    return vstack(plotsh...), vstack(plotse...), vstack(plotsb...)
end

if length(ARGS) >= 3
    path = ARGS[1]
    plotpath = ARGS[2]
    if !isdir(plotpath)
        mkpath(plotpath)
    end
    xname = ARGS[3]
    df2 = makeframe(path)
else
    printexit()
end

if length(ARGS) == 3
    p = plot(df2, x=xname, ygroup="Particle", color="Energy", Geom.subplot_grid(Geom.density))
    Gadfly.draw(PNG("$(plotpath)$(xname).png", 14cm, 14cm), p)
elseif length(ARGS) == 4 && ARGS[4] == "-s" #s stands for scale
    p = plot(df2, x=xname, ygroup="Particle", color="Energy", Geom.subplot_grid(Geom.density, Scale.x_log10))
    Gadfly.draw(PNG("$(plotpath)$(xname)-logplot.png", 14cm, 14cm), p)
elseif length(ARGS) == 4
    yname = ARGS[4]
    p = plot(df2, x=xname, y=yname, xgroup="Particle", ygroup="Energy", Geom.subplot_grid(Geom.histogram2d(xbincount=30, ybincount=30), Guide.xticks(ticks=[0., 1.]), Guide.yticks(ticks=[0., 1.])), Guide.xlabel("$xname by Particle"), Guide.ylabel("$yname by Energy (GeV)"))
    Gadfly.draw(PNG("$(plotpath)$(yname)_vs_$(xname).png", 14cm, 14cm), p)
elseif length(ARGS) == 5 && ARGS[5] == "-s" #s stands for separate
    yname = ARGS[4]

    dfh = df2[df2[:Ecount] .< (df2[:Ecount] + df2[:Hcount]) / 100, :]
    dfe = df2[df2[:Hcount] .< (df2[:Ecount] + df2[:Hcount]) / 100, :]
    dfb = df2[(df2[:Ecount] .> (df2[:Ecount] + df2[:Hcount]) / 100) & (df2[:Hcount] .> (df2[:Ecount] + df2[:Hcount]) / 100), :]

    plots = map(x->plotparticle(x, xname, yname), allparticles)
    plotsh = Array(Any, length(plots))
    plotse = Array(Any, length(plots))
    plotsb = Array(Any, length(plots))
    for i=1:length(plots)
      plotsh[i] = plots[i][1]
      plotse[i] = plots[i][2]
      plotsb[i] = plots[i][3]
    end
    Gadfly.draw(PNG("$(plotpath)$(yname)_vs_$(xname)-h.png", 36cm, 36cm), hstack(plotsh...))
    Gadfly.draw(PNG("$(plotpath)$(yname)_vs_$(xname)-e.png", 36cm, 36cm), hstack(plotse...))
    Gadfly.draw(PNG("$(plotpath)$(yname)_vs_$(xname)-b.png", 36cm, 36cm), hstack(plotsb...))
else
    printexit()
end
