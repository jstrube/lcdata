include("fractal.jl")

using MeshTools
using LCIOUtils
using HDF5
using DataStructures
const globalscale = 1
if length(ARGS) != 12
    println("Usage: PROGRAM filename outputdir collection startevent endevent resX resY resZ blurRadius buffer surfacethreshold volumethreshold")
    println("For the events in the specified range, convert the hit data to an energy density scalar field with the specified
    extended border, blur by the specified amount, create an isosurface with the specified threshold, then write various pieces of information
    to files based on the generated mesh.")
    println("hulldensity: total energy divided by volume of convex hull of mesh")
    println("meshdensity: total energy divided by volume of mesh")
    println("hulleccentricity: volume to surface area ratio of convex hull divided by volume to surface area ratio of a sphere of the same volume")
    println("mesheccentricity: volume to surface area ratio of mesh divided by volume to surface area ratio of a sphere of the same volume")
    println("volumeratio: volume of mesh divided by volume of convex hull")
    println("MCEnergy: reconstructed energy of this particle")
    println("meshparts: Number of mesh parts with volume greater than volumethreshold")

    println("eigenvalue: Smallest moment of inertia, then the two larger ones. The former corresponds to the main axis")
    exit()
end

const filename = ARGS[1]
const outputdir = ARGS[2]
if !isfile(filename)
    println("file not found: $filename")
    exit()
end
if !isdir(outputdir)
    mkpath(outputdir)
end
const expr = r".*/(.*).slcio"
if !ismatch(expr, filename)
    println("not an slcio file!")
    exit()
end
const outputName = match(expr, filename)[1]

const collectionname = ARGS[3]
const eventstart = parse(Int, ARGS[4])
const eventend = parse(Int, ARGS[5])
const resX = parse(Int, ARGS[6])
const resY = parse(Int, ARGS[7])
const resZ = parse(Int, ARGS[8])
const stdev = parse(Float64, ARGS[9])
const buffer = parse(Float64, ARGS[10])
const globalthreshold = parse(Float64, ARGS[11])
const volthreshold = parse(Float64, ARGS[12])

const basebasename = joinpath(outputdir,"$(outputName)-$(collectionname)")
const basename =  "$basebasename-$(resX)x$(resY)x$(resZ)-blur_$(stdev)-buffer_$(buffer)-threshold_$(globalthreshold)"
const name = "$basename-volthreshold_$(volthreshold)"

const longsteps = 40
const latsteps = 20
const maxrad = 1000

type Collector
    densityfilename
    ratiofilename
    meshdensityfilename
    mesheccentricityfilename
    mhulleccentricityfilename
    numpartsfilename
    energyfilename
    numhitsfilename
    calcountsfilename
    energiesfilename
    fractaldimsfilename
    eigenfilename
    eigenvfilename
    anglefilename
    datafilename
    totalEfilename

    positions :: LinkedList{Matrix}
    length :: Int
    particle :: Int
    event :: Int
    function Collector()
        new(
        "$name-hulldensity.txt",
        "$name-volumeratio.txt",
        "$name-meshdensity.txt",
        "$name-mesheccentricity.txt",
        "$name-hulleccentricity.txt",
        "$name-meshparts.txt",
        "$basebasename-MCEnergy.txt",
        "$basebasename-numhits.txt",
        "$basebasename-calcounts.txt",
        "$basebasename-energydist.h5",
        "$basebasename-fractaldim.txt",
        "$basebasename-eigenvalue.txt",
        "$basebasename-eigenvector.txt",
        "$basebasename-angle.txt",
        "$basebasename-data.txt",
        "$basebasename-totalE.txt",
        nil(Matrix), 0, 1, 0)
    end
end

import Base.call

"""
    sliceenergies, mindist, maxdist, centroid, maxrad = distribute(positions, dir)
bins the weighted points `positions` into a 2D array where row corresponds to
radial distance from the ray `dir` centered at the weighted centroid of the positions
and column corresponds to distance along the axis `dir`.
"""
function distribute(positions, dir)
    centroid = (sum(positions[1:3,:] .* positions[4,:], 2)[:,1] / sum(positions[4,:]))::Vector
    # dir = dir/norm(dir)
    positions_centered = positions[1:3,:] - repeat(centroid, inner=[1, size(positions)[2]])::Matrix
    dists   = mapslices(v->dot(v, dir), positions_centered, 1)
    mindist = minimum(dists)
    maxdist = maximum(dists)

    delta = maxdist - mindist
    # println("$miny, $dy")
    sliceenergies = zeros(Float64, latsteps, longsteps)
    for j=1:size(positions)[2]
        p = positions_centered[:, j]
        slice = convert(Int, clamp(ceil((dot(p, dir) - mindist)/delta * longsteps), 1, longsteps))
        ring = convert(Int, clamp(ceil(norm(p - dot(p, dir) * dir) / maxrad * latsteps), 1, latsteps))
        sliceenergies[ring, slice] += positions[4, j]
    end
    return sliceenergies, mindist, maxdist, centroid, maxrad
end

function call(collector :: Collector, positions :: Matrix, i :: Int, maxEnergy :: Float64, momentum :: Vector, particleID)
    numhits = size(positions)[2]
    if numhits == 0
        println("no points to load at event $(i)!")
        maxEnergy = -1
        mhull_energydensity = -1
        ratio = -1
        mesh_energydensity = -1
        mhulleccentricity = -1
        mesheccentricity = -1
        numparts = 0
        mindist = -1
        maxdist = -1
        maxrad = -1
        dir = [0, 0, 0]
        centroid = [0, 0, 0]
        # sliceenergies = fill(-1, latsteps, longsteps)
        hcount = 0
        ecount = 0
        face = -1
        fracdim = -1
        v = w1 = w2 = 0
        axis = [0 0 0]
        angle = -1
        totalE = -1
    else
        print("processing event $i with $numhits hits... ")
        totalE = sum(positions[4,:])
        #collector.positions = cons(positions, collector.positions)
        collector.length += numhits
        if i == collector.event
            collector.particle += 1
        else
            collector.particle = 1
            collector.event = i
        end

        #compute fractal dimension
        fracdim = fractaldim(positions[1:3,:], 3, 10)

        inertia = inertiatensor(positions)
        F = eigfact(inertia)
        v, j = findmin(F[:values])
        w1, w2 = F[:values][[1:j-1; j+1:3]]
        axis = F[:vectors][:,j]
        axis/=norm(axis)
        cosangle = abs(dot(axis, momentum)/norm(momentum))
        angle = acos(cosangle)

        #processing radial distribution of energy along principle axis
        # sliceenergies, mindist, maxdist, centroid, maxrad = distribute(positions, axis)
        #use dir and centroid to find intersection with cal border, and orientation of the calorimeter
        centroid = (sum(positions[1:3,:] .* positions[4,:], 2)[:,1] / sum(positions[4,:]))
        theta = (atan2(centroid[1], centroid[2]) + 2pi) % 2pi
        face = convert(Int, round(theta/2pi * numfaces))
        facetheta = face/numfaces * 2pi

        normal = [sin(facetheta), 0, cos(facetheta)]
        ecount = hcount = 0
        for j=1:size(positions)[2]
            #don't accidentally scale the energy too
            rad = dot(normal, positions[1:3, j])
            if rad > ecal_outer
                #it's in the hadronic calorimeter
                hcount += 1
            else
                #it's in the electromagnetic calorimeter
                ecount += 1
            end
        end
        # println("face: $face")
        # println("ang: $(theta)")
        # println(centroid)
        grid = MeshGrid(resX, resY, resZ, positions, true, stdev, [buffer, buffer, buffer], globalthreshold)

        print(".")

        verts, indices = createMesh(grid, globalthreshold, 1, globalscale)
        if size(verts)[2] == 0
            println("no points in mesh")
        else
            print(".")
            verts, indices = removeDoubles(verts, indices)
            print(".")
            parts = separate(indices)
            numparts = 0
            for (i,part) in enumerate(parts)
                vol = volume(verts, part)
                if vol > volthreshold
                    numparts += 1
                end
            end
            print(".")
            verts_mhull, indices_mhull = convexhull(verts)
            print(".")
            meshvol = volume(verts, indices)
            meshsurf = surfacearea(verts, indices)
            mhullvol = volume(verts_mhull, indices_mhull)
            mhullsurf = surfacearea(verts_mhull, indices_mhull)

            totalE = sum(positions[4, :])
            mhull_energydensity = totalE/mhullvol
            ratio = meshvol/mhullvol
            mesh_energydensity = totalE/meshvol
            mhulleccentricity = mhullvol / mhullsurf / cbrt( 3/(4pi) * mhullvol) * 3
            mesheccentricity = meshvol / meshsurf / cbrt( 3/(4pi) * meshvol) * 3
            print(".\n")
        end
    end

    append(collector.densityfilename, "$(mhull_energydensity)\n")
    append(collector.ratiofilename, "$(ratio)\n")
    append(collector.meshdensityfilename, "$(mesh_energydensity)\n")
    append(collector.mhulleccentricityfilename, "$(mhulleccentricity)\n")
    append(collector.mesheccentricityfilename, "$(mesheccentricity)\n")
    append(collector.numpartsfilename, "$(numparts)\n")
    append(collector.energyfilename, "$(maxEnergy)\n")
    append(collector.numhitsfilename, "$numhits\n")
    append(collector.calcountsfilename, "$ecount $hcount $face\n")
    append(collector.fractaldimsfilename, "$(fracdim)\n")
    # h5write(collector.energiesfilename, "event$i/particle$collector.particle/dist", sliceenergies)
    # h5write(collector.energiesfilename, "event$i/particle$collector.particle/mindist", mindist)
    # h5write(collector.energiesfilename, "event$i/particle$collector.particle/maxdist", maxdist)
    # h5write(collector.energiesfilename, "event$i/particle$collector.particle/dir", axis)
    # h5write(collector.energiesfilename, "event$i/particle$collector.particle/centroid", centroid)
    # h5write(collector.energiesfilename, "event$i/particle$collector.particle/maxrad", maxrad)
    append(collector.eigenfilename, "$v $w1 $w2\n")
    append(collector.eigenvfilename, "$(axis[1]) $(axis[2]) $(axis[3])\n")
    append(collector.anglefilename, "$angle\n")
    append(collector.datafilename, "$(i) $(particleID)\n")
    append(collector.totalEfilename, "$totalE\n")
end
collector = Collector()
mappositions(collector, filename, eventstart, eventend)
positions = Array(Float64, 4, collector.length)
ind = 1
for points in collector.positions
    len = size(points)[2]
    positions[:,ind:ind+len-1] = points
    ind += len
end

# inertia = inertiatensor(positions)
# F = eigfact(inertia)
# v, j = findmin(F[:values])
# w1, w2 = F[:values][[1:j-1; j+1:3]]
# axis = F[:vectors][:,j]
# axis/=norm(axis)

# sliceenergies, mindist, maxdist, centroid, maxrad = distribute(positions, axis)
# h5write(collector.energiesfilename, "dist", sliceenergies)
# h5write(collector.energiesfilename, "mindist", mindist)
# h5write(collector.energiesfilename, "maxdist", maxdist)
# h5write(collector.energiesfilename, "dir", axis)
# h5write(collector.energiesfilename, "centroid", centroid)
# h5write(collector.energiesfilename, "maxrad", maxrad)
# h5write(collector.energiesfilename, "eig", [v, w1, w2])
