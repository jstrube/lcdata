include("LCIOUtils.jl")
using MeshTools, LCIOUtils, HDF5

if length(ARGS) != 8
    println("usage: PROGRAM filename outdir startevent endevent resx resy resz stdev")
end

const filename = ARGS[1]
if !isfile(filename)
    println("file not found: $filename")
    exit()
end
const expr = r".*/(.*).slcio"
if !ismatch(expr, filename)
    println("not an slcio file!")
    exit()
end
const outputName = match(expr, filename)[1]
const outputdir = ARGS[2]
const startevent = parse(Int, ARGS[3])
const endevent = parse(Int, ARGS[4])
const resx = parse(Int, ARGS[5])
const resy = parse(Int, ARGS[6])
const resz = parse(Int, ARGS[7])
const stdev = parse(Float64, ARGS[8])

const basename = joinpath(outputdir,"$outputName-$(resx)x$(resy)x$(resz)-blur_$(stdev)")

import Base.call
function g(positions::Matrix, event::Int, maxEnergy, momentum, particletype)
    numhits = size(positions, 2)
    if numhits == 0
        println("no points to load at event $(event)!")
    else
        print("processing event $(event)... ")
        grid = MeshGrid(resx, resy, resz, positions, true, stdev)
        densities = Array(Float64, numhits)
        for i=1:numhits
            densities[i] = grid[positions[1:3,i]...]
        end
        h5write("$basename-densities.h5", "event$event", densities)
    end
end

mappositions(g, filename, startevent, endevent)
