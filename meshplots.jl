include("lcioanalyze.jl")
df = makeframe(path)

function makemesh(name, energy)
    println("processing $(name) with energy $(energy)...")
    df1 = df[(df[:Particle] .== name) & (df[:Energy] .== energy) & (df[:VolumeRatio] .!= -1), :]
    positions = Array(Float64, 4, size(df1)[1])
    positions[1, :] = df1[:VolumeRatio]
    positions[2, :] = df1[:MeshEccentricity]
    positions[3, :] = df1[:HullEccentricity]
    positions[4, :] = 1
    println("creating grid...")
    grid = MeshGrid(Float64[0, 0, 0], Float64[1, 1, 1], 25, 25, 25, positions, true, 0.05, Float64[0,0,0])
    maxval = maximum(grid.data)
    minval = minimum(grid.data)
    println("creating mesh...")
    v, ind = createMesh(grid, (maxval-minval)*0.5 + minval)
    println("removing doubles...")
    v, ind = removeDoubles(v, ind)
    println("saving mesh...")
    saveObj(v, ind, "$(plotpath)thresh50$(name)-$(energy).obj")
end

function makemesh(name)
    println("processing $(name)...")
    df1 = df[(df[:Particle] .== name) & (df[:VolumeRatio] .!= -1), :]
    positions = Array(Float64, 4, size(df1)[1])
    positions[1, :] = df1[:VolumeRatio]
    positions[2, :] = df1[:MeshEccentricity]
    positions[3, :] = df1[:HullEccentricity]
    positions[4, :] = 1
    println("creating grid...")
    grid = MeshGrid(Float64[0, 0, 0], Float64[1, 1, 1], 25, 25, 25, positions, true, 0.05, Float64[0,0,0])
    maxval = maximum(grid.data)
    minval = minimum(grid.data)
    println("creating mesh...")
    v, ind = createMesh(grid, (maxval-minval)*0.5 + minval)
    println("removing doubles...")
    v, ind = removeDoubles(v, ind)
    println("saving mesh...")
    saveObj(v, ind, "$(plotpath)thresh50$(name).obj")
end

for name=["pi-", "mu+", "gamma", "e-", "K0L"]
  for energy=[1, 2, 5, 10, 20, 50, 100]
    makemesh(name, energy)
  end
end

for name=["pi-", "mu+", "gamma", "e-", "K0L"]
  makemesh(name)
end
