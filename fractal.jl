using DataStructures
include("linesegments.jl")
const mindisp = 0.1

"""
    fractaldim(points, mincount=1, maxcount=20)

Approximate the Hausdorff dimension of a set of points using the box-counting algorithm.
`mincount`: minimum number of data points
`maxcount`: maximum number of data points
each data point refers to a (box count, box sidelength) pair with a different resolution grid. The grid resolution doubles with each iteration,
and the slope of the log-log data is used as the dimension.
"""
function fractaldim(points, mincount=1, count=20)
    minPt = minimum(points, 2)[:,1]
    maxPt = maximum(points, 2)[:,1]
    disp = (maxPt - minPt)
    for i=1:length(disp)
        if disp[i] == 0
            disp[i] = mindisp
        end
    end
    ns = Array(Int, count)
    ress = Array(Int, count)
    res = 1
    maxcount = 0
    lastn = -1

    for i=1:count
        maxcount += 1
        data = zeros(Int, fill(res, size(points, 1))...)
        for j=1:size(points, 2)
            pos = points[:, j]
            realPos = (pos - minPt) ./ disp * res
            realind = clamp(ceil(Int, realPos), 1, res)
            if data[realind...] != 1
                data[realind...] = 1
            end
        end
        ns[i] = sum(data)
        ress[i] = res
        if i >= mincount && ns[i] == lastn
            break
        end
        res *= 2
        lastn = ns[i]
    end
    m1, m2 = twosegmentfit(log(ress[1:maxcount]), log(ns[1:maxcount]))
    return m1#, ress[1:maxcount], ns[1:maxcount]
end
