#!/bin/bash

if [[ $# < 1 ]]
then
    echo "Run this script with the path to your slcio files"
    exit 1
fi

for x in $1*single[megKp]*scint1x1*_theta90*_phi90*_REC.slcio
do 
nohup /scratch/software/julia-intel/julia lciohist.jl $x data/output PandoraPFOCollection 1 1000 25 25 25 8 0.5 0.01 5 > output/$(basename ${x/slcio/out}) 2>&1 ;
done
