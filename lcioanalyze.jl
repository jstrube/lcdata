using DataFrames
using HDF5
using LsqFit

const params = "25x25x25-blur_8.0-buffer_0.5-threshold_0.01-volthreshold_5.0-"
getoriginalname(name, energy) = "single$(name)_$(energy)__scint1x1_$(name)_theta90_phi90_REC"
getbasename(name, energy) = "$(getoriginalname(name, energy))-PandoraPFOCollection"
getfilename(name, energy, property, fullname=true, extension="txt") = "$(getbasename(name, energy))-$( fullname ? params : "")$(property).$extension"

getdensityfilename(name, energy, blur::Float64, startevent, endevent) = "$(getoriginalname(name, energy))-25x25x25-blur_$blur-densities.h5"

const allparticles = ["gamma", "e-", "mu+", "K0L", "pi-"]
const allenergies = [1, 2, 5, 10, 20, 50, 100]
const names = Dict([(13, "mu-"), (-13, "mu+"), (11, "e-"), (-11, "e+"), (211, "pi+"), (-211, "pi-"), (22, "gamma"), (2112, "n")])

"Make a dataframe of density distributions for the specified particle and energy"
function getparticledensitydist(name, energy, prefix, blur, startevent, endevent)
    filename = joinpath(prefix,"$(getdensityfilename(name, energy, blur, startevent, endevent))")
    if isfile(filename)
        df = DataFrame()
        hfile = h5open(filename, "r")
        for i=startevent:endevent
            if exists(hfile, "event$i")
                hits = readmmap(hfile["event$i"])
                df = [df ; DataFrame(Any[hits, fill(i, length(hits))], [:Density, :Event])]
            end
        end
        close(hfile)
        return Nullable(df)
    else
        println("$filename not found")
        return Nullable()
    end
end

"Make a dataframe of density distributions for all particles at all energies"
function makedensitydistframe(prefix, blur, startevent=1, endevent=1000)
    df = DataFrame()
    for name in allparticles
        for energy in allenergies
            miniframe = getparticledensitydist(name, energy, prefix, blur, startevent, endevent)
            if !isnull(miniframe)
                miniframe = get(miniframe)
                df = [df; hcat(miniframe, DataFrame(Energy=fill(energy, size(miniframe, 1)), Particle=fill(name, size(miniframe, 1))))] :: DataFrame
            end
        end
    end
    return df
end

"Make a dataframe of the given property for the specified particle and energy"
function getparticledata(name, energy, property, prefix, startevent::Int, endevent::Int, fullname=true, T=Float64)
    filename = joinpath(prefix,"$(getfilename(name, energy, property, fullname))")
    if isfile(filename)
        data = readdlm(filename, T)
        return Nullable(data[startevent:min(size(data, 1), endevent), :])
    else
        println("$filename not found")
        return Nullable()
    end
end

function getdistfilename(name, energy, prefix)
    return "$prefix$(getfilename(name, energy, "energydist", false, "h5"))"
end

"Collect the data for the given particles with all available energies"
function makeparticleframe(name, prefix, startevent, endevent)
    df = DataFrame()
    found = false
    for i=allenergies
        data_hd = getparticledata(name, i, "hulldensity", prefix, startevent, endevent)
        data_md = getparticledata(name, i, "meshdensity", prefix, startevent, endevent)
        data_he = getparticledata(name, i, "hulleccentricity", prefix, startevent, endevent)
        data_me = getparticledata(name, i, "mesheccentricity", prefix, startevent, endevent)
        data_vr = getparticledata(name, i, "volumeratio", prefix, startevent, endevent)
        data_mp = getparticledata(name, i, "meshparts", prefix, startevent, endevent)
        data_fd = getparticledata(name, i, "fractaldim", prefix, startevent, endevent, false)
        data_ev = getparticledata(name, i, "eigenvalue", prefix, startevent, endevent, false)
        data_calcounts = getparticledata(name, i, "calcounts", prefix, startevent, endevent, false, Int)
        data_recenergy = getparticledata(name, i, "MCEnergy", prefix, startevent, endevent, false)
        data_totalenergy = getparticledata(name, i, "totalE", prefix, startevent, endevent, false)

        if !isnull(data_hd) && !isnull(data_md) && !isnull(data_he) && !isnull(data_me) && !isnull(data_vr) && !isnull(data_mp) && !isnull(data_fd) && !isnull(data_calcounts) && !isnull(data_ev) && !isnull(data_recenergy) && !isnull(data_totalenergy)
            df = [df; DataFrame(Any[get(data_hd), get(data_md), get(data_he), get(data_me), get(data_vr), get(data_mp), get(data_fd), get(data_ev)[:,1], get(data_calcounts)[:,1], get(data_calcounts)[:,2], get(data_calcounts)[:,3], fill(i, length(get(data_hd))), get(data_recenergy), get(data_totalenergy)], [:HullDensity, :MeshDensity, :HullEccentricity, :MeshEccentricity, :VolumeRatio, :MeshParts, :FractalDimension, :Inertia, :Ecount, :Hcount, :CalIndex, :Energy, :MCEnergy, :TotalEnergy])]
            found = true
        end
    end
    if found
        return Nullable(df)
    else
        return Nullable()
    end
end

"""
    makeframe(prefix, startevent=1, endevent=1000)

Collect the data for all available particles at all available energies.
`prefix`: the path to the directory containing the data files for all particles at all energies

"""
function makeframe(prefix, startevent=1, endevent=typemax(Int))
    df = DataFrame()
    for name = allparticles
        newdf = makeparticleframe(name, prefix, startevent, endevent)
        if !isnull(newdf)
            namedf = DataFrame(Any[fill(name, size(get(newdf))[1])], [:Particle])
            df = [df; [get(newdf) namedf]]
        end
    end
    return df[df[:VolumeRatio] .!= -1, :]
end

"Collect the data from a particle shower"
function makeshowerframe(filename)
    df = DataFrame()
    data = readdlm("$(filename)data.txt", Int)
    data_hd = readdlm("$(filename)$(params)hulldensity.txt")
    data_he = readdlm("$(filename)$(params)hulleccentricity.txt")
    data_md = readdlm("$(filename)$(params)meshdensity.txt")
    data_me = readdlm("$(filename)$(params)mesheccentricity.txt")
    data_mp = readdlm("$(filename)$(params)meshparts.txt", Int)
    data_vr = readdlm("$(filename)$(params)volumeratio.txt")
    data_ang = readdlm("$(filename)angle.txt")
    data_cc = readdlm("$(filename)calcounts.txt", Int)
    data_eval = readdlm("$(filename)eigenvalue.txt")
    # data_evec = readdlm("$filename-eigenvector.txt")
    data_fd = readdlm("$(filename)fractaldim.txt")
    data_e = readdlm("$(filename)MCEnergy.txt")
    data_n = readdlm("$(filename)numhits.txt", Int)
    df[:Particle] = map(x->names[x], data[:, 2])
    df[:Event] = data[:, 1]
    df[:HullDensity] = data_hd[:, 1]
    df[:MeshDensity] = data_md[:, 1]
    df[:HullEccentricity] = data_he[:, 1]
    df[:MeshEccentricity] = data_me[:, 1]
    df[:MeshParts] = data_mp[:, 1]
    df[:VolumeRatio] = data_vr[:, 1]
    df[:Angle] = data_ang[:, 1]
    df[:ECount] = data_cc[:, 1]
    df[:HCount] = data_cc[:, 2]
    df[:Face] = data_cc[:, 3]
    df[:EigenValue] = data_eval[:, 1]
    df[:FractalDimension] = data_fd[:, 1]
    df[:Energy] = data_e[:, 1]
    df[:HitCount] = data_n[:, 1]
    return df[df[:VolumeRatio] .!= -1, :]
end

function makedensityframe(filename)
    df = DataFrame()
    lo = readdlm("$(filename)low.txt")
    hi = readdlm("$(filename)high.txt")
    al = readdlm("$(filename)all.txt")
    th = readdlm("$(filename)threshold.txt")

    df[:Low] = lo[:,1]
    df[:High] = hi[:,1]
    df[:All] = al[:,1]
    df[:Threshold] = th[:,1]
    return df
end
