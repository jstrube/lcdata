using Gadfly
include("lcioanalyze.jl")
path = ARGS[1]
plotpath = ARGS[2]

df = makeframe(path)

df[:RelativeError] = (df[:MCEnergy] - df[:Energy]) ./ df[:Energy]

p = plot(df, x="Energy", y="RelativeError", ygroup="Particle", Geom.subplot_grid(Geom.point))
draw(PNG("$(plotpath)errors.png", 18cm, 18cm), p)
