
using MeshTools
using LCIOUtils

macro reportprogress(ex, msg, printresult)
  return quote
    println($msg)
    local a = time()
    local val = $ex
    local b = time()
    print("finished in $(b-a) seconds.")
    if $printresult print("\nresult: $(val)") end
    print("\n\n")
    val
  end
end

const globalscale = 0.04

if length(ARGS) != 10
  println("Usage: PROGRAM filename outputdir collection event resX resY resZ blurRadius buffer globalthreshold")
  exit()
end

const filename = ARGS[1]
const outputdir = ARGS[2]
if !isfile(filename)
  println("file not found: $filename")
  exit()
end
if !isdir(outputdir)
    mkpath(outputdir)
end
const expr = r".*/(.*).slcio"
if !ismatch(expr, filename)
  println("not an slcio file!")
  exit()
end
const outputName = match(expr, filename)[1]

const collectionname = ARGS[3]
const eventNum = parse(Int, ARGS[4])
const resX = parse(Int, ARGS[5])
const resY = parse(Int, ARGS[6])
const resZ = parse(Int, ARGS[7])
const stdev = parse(Float64, ARGS[8])
const buffer = parse(Float64, ARGS[9])
const globalthreshold = parse(Float64, ARGS[10])

const basebasename = joinpath(outputdir, "$(outputName)-$(collectionname)-event_$(eventNum)")
const basename =  "$basebasename-$(resX)x$(resY)x$(resZ)-blur_$(stdev)-buffer_$(buffer)-threshold_$(globalthreshold)"

positions = @reportprogress getpositions(filename, collectionname, eventNum) "getting hit data..." false
if size(positions)[2] == 0
  println("no points to load!")
  exit()
end
println("hits: $(size(positions)[2])")
grid = @reportprogress MeshGrid(resX, resY, resZ, positions, true, stdev, [buffer, buffer, buffer], globalthreshold) "creating grid..." false

verts, indices = @reportprogress createMesh(grid, globalthreshold, 1, globalscale) "creating mesh..." false
if size(verts)[2] == 0
  println("no points in mesh!")
  exit()
end
println("points in raw mesh: $(size(verts)[2])")

verts, indices = @reportprogress removeDoubles(verts, indices, 1e-8) "removing doubles..." false
println("points in simplified mesh: $(size(verts)[2])")

parts = @reportprogress separate(indices) "finding distinct mesh parts..." false
println("$(length(parts)) parts")
vols = map(x->(volume(verts, x), x), parts)
vols = sort([vols...], rev=true)
len = length(vols)
print("volumes of parts: ")
for (i, part) in enumerate(vols)
    if i < len
        print("$(part[1]), ")
    else
        println(part[1])
    end
    saveObj(verts, part[2], "$(basename)-part_$i.obj")
end
verts_hull, indices_hull = @reportprogress convexhull(positions[1:3, :] * globalscale) "computing convex hull..." false
verts_mhull, indices_mhull = @reportprogress convexhull(verts) "computing mesh convex hull..." false

meshvol = volume(verts, indices)
hullvol = volume(verts_hull, indices_hull)
mhullvol = volume(verts_mhull, indices_mhull)
println("volume of mesh: $(meshvol)")
println("volume of hull: $(hullvol)")
println("volume of mesh hull: $(mhullvol)")
totalE = sum(positions[4, :])

meshsurf = surfacearea(verts, indices)
hullsurf = surfacearea(verts_hull, indices_hull)
mhullsurf = surfacearea(verts_mhull, indices_mhull)
println("surface area of mesh: $(meshsurf)")
println("surface area of hull: $(hullsurf)")
println("surface area of mesh hull: $(mhullsurf)")

println("mesh to hull volume ratio: $(meshvol/hullvol)")
println("mesh to mesh hull volume ratio: $(meshvol/mhullvol)")
println("energy density: $(totalE/mhullvol)")
println("mesh hull volume to surface vs. sphere: $(mhullvol / mhullsurf / cbrt( 3/(4pi) * mhullvol) * 3)")
println("mesh volume to surface vs. sphere: $(meshvol / meshsurf / cbrt( 3/(4pi) * meshvol) * 3)\n")

# println("mesh to mesh hull surface area ratio: ")

@reportprogress saveObj(verts, indices, "$(basename).obj") "saving volume mesh..." false
@reportprogress saveObj(verts_hull, indices_hull, "$(basebasename)-convex_hull.obj") "saving convex hull..." false
@reportprogress saveObj(verts_mhull, indices_mhull, "$(basename)-convex_mhull.obj") "saving mesh convex hull..." false
