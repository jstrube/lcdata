using CurveFit

function twosegmentfit(x, y)
  len = length(x)
  mincost = Inf
  mini = 1
  optslopes = 0, 0
  for i=2:len
    cost = 0
    b, m1 = linear_fit(x[1:i], y[1:i])
    err = x[1:i] * m1 + b - y[1:i]
    cost += dot(err, err)
    b, m2 = linear_fit(x[i:len], y[i:len])
    err = x[i:len] * m2 + b - y[i:len]
    cost += dot(err, err)

    if cost < mincost
      mincost = cost
      mini = i
      optslopes = m1, m2
    end
  end
  # println("optimum i: $mini")
  return optslopes
end
function segmentfit(x, y, C)
  x, y = reverse(x), reverse(y)
  len = length(x)
  eij = zeros(Float64, len, len)
  for i=1:len
    for j=i+1:len
      b, m = linear_fit(x[i:j], y[i:j])
      err = x[i:j] * m + b - y[i:j]
      eij[i,j] = dot(err, err)
    end
  end
  opt = Array(Float64, len)
  opt[1] = 0
  js = Array(Int, len)
  segments = Array(Int, len)
  segments[1] = 0
  for i=2:len
    mincost = Inf
    minj = 1
    for j=1:i-1
      cost = opt[j] + C + eij[j, i]
      if cost < mincost
        mincost = cost
        minj = j
      end
    end
    opt[i] = mincost
    js[i] = minj
    segments[i] = segments[minj] + 1
  end
  # println("segments: $(segments[end])")
  b, m = linear_fit(x[js[end]:end], y[js[end]:end])
  return m
end
