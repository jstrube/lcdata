include("LCIOUtils.jl")
using MeshTools, LCIOUtils, DataStructures, Optim

if length(ARGS) != 10
    println("usage: PROGRAM filename outdir startevent endevent resx resy resz stdev A B")
end

const filename = ARGS[1]
if !isfile(filename)
    println("file not found: $filename")
    exit()
end
const expr = r".*/(.*).slcio"
if !ismatch(expr, filename)
    println("not an slcio file!")
    exit()
end
const outputName = match(expr, filename)[1]
const outputdir = ARGS[2]
if !isdir(outputdir)
    mkpath(outputdir)
end
const startevent = parse(Int, ARGS[3])
const endevent = parse(Int, ARGS[4])
const resx = parse(Int, ARGS[5])
const resy = parse(Int, ARGS[6])
const resz = parse(Int, ARGS[7])
const stdev = parse(Float64, ARGS[8])
const A = parse(Float64, ARGS[9]) # multiplier for low points
const B = parse(Float64, ARGS[10]) #mulitplier for high points

const basename = joinpath(outputdir,"$outputName-$(resx)x$(resy)x$(resz)-blur_$(stdev)-a_$A-b_$B")


type OptRecord
    points :: Matrix{Float64}
    grid :: MeshGrid
    minE :: Float64
    maxE :: Float64
    evals :: Int
    energy :: Float64
    lowtotal :: Float64
    hightotal :: Float64
    function OptRecord(points, grid, energy)
        minE = Inf
        maxE = -Inf
        for i=1:size(points, 2)
            val = grid[points[1:3,i]...]
            if val > maxE
                maxE = val
            end
            if val < minE
                minE = val
            end
        end
        new(points, grid, minE, maxE, 0, energy)
    end
end

function call(record::OptRecord, thresh)
    record.evals += 1
    hicount = 0
    locount = 0
    hightotal = 0.0
    lowtotal = 0.0
    hipoints = nil(Float64)
    lopoints = nil(Float64)
    threshold = thresh * record.maxE + (1-thresh) * record.minE
    # println("minE: $(record.minE)")
    # println("maxE: $(record.maxE)")
    # println("threshold: $(threshold)")
    for i=1:size(record.points, 2)
        if record.grid[record.points[1:3,i]...] > threshold
            hicount += 1
            hightotal += record.points[4,i]
            hipoints = cons(record.points[4,i], hipoints)
        else
            locount += 1
            lowtotal += record.points[4,i]
            lopoints = cons(record.points[4,i], lopoints)
        end
    end
    hipoints = [hipoints...]
    lopoints = [lopoints...]
    record.lowtotal = lowtotal
    record.hightotal = hightotal

    # println("hicount: $(hicount)")
    # println("locount: $(locount)")
    # println("histd: $(histd)")
    # println("lostd: $(lostd)")
    return (sum(hipoints) * B + sum(lopoints) *  A - record.energy)^2
end

# norm2(v) = dot(v, v)
# stdn(v) = sqrt(mean(mapslices(norm2, v, 1)) - norm2(mean(v, 2)[:,1]))

function f(positions::Matrix, event::Int, maxEnergy, momentum, particletype)
    numhits = size(positions, 2)
    lowtotal = 0
    hightotal = 0
    optthreshold = -1
    if numhits == 0
        println("no points to load at event $(event)!")
    else
        print("processing event $(event)... ")
        grid = MeshGrid(resx, resy, resz, positions, true, stdev)
        record = OptRecord(positions, grid, maxEnergy)
        closure(x) = record(x)

        optthreshold = optimize(closure, [-0.0, 1.0], method=GoldenSection(), iterations=66)


        lowtotal = record.lowtotal
        hightotal = record.hightotal
        print("steps: $(record.evals)... ")
        println("Residual: $(closure(res))")
    end
    append("$basename-low.txt", "$lowtotal\n")
    append("$basename-high.txt", "$hightotal\n")
    append("$basename-all.txt", "$(lowtotal+hightotal)\n")
    append("$basename-threshold.txt", "$res\n")
    # append("$basename-energyratio.txt", "$(50/maxEnergy)\n")
end

mappositions(f, filename, startevent, endevent)
