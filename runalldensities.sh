#!/bin/bash


for x in /hep/ILC/users/stru821/StatisticalAnalysis/*single[megKp]*scint1x1*_theta90*_phi90*_REC.slcio
do 
nohup /scratch/software/julia-intel/julia lciodensitydist.jl $x data/densityoutput/ 1 1000 25 25 25 16 > densityoutput/$(basename ${x/slcio/out}) 2>&1 ;
done
