using DataStructures
function sierpinski(count)
  width = 100
  height = 100
  vertex1 = [width/2, 0]
  vertex2 = [width, height]
  vertex3 = [0, height]
  currentposition = [rand() * width, rand() * height]
  points = nil(Vector{Float64})
  for i=1:count
    points = cons(currentposition, points)
    selected = rand(Any[vertex1, vertex2, vertex3])
    currentposition = (currentposition + selected)/2
  end
  pointsarr = Array(Float64, 2, length(points))
  for (i, pt) in enumerate(points)
    pointsarr[:,i] = pt
  end
  return pointsarr
end
